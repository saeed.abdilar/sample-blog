import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../_models/user';
import { Post } from '../_models/post';

@Injectable()
export class FakeBackend implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const savedUsers: User[] = JSON.parse(localStorage.getItem('users')) || [];
        const savedPosts: Post[] = JSON.parse(localStorage.getItem('posts')) || [];

        return of(null).pipe(mergeMap(() => {
            if (request.url.endsWith('/authenticate') && request.method === 'POST') {
                const users = savedUsers.filter(user => {
                    return user.email == request.body.email && user.password == request.body.password;
                });
                if (users.length) {
                    const body = {
                        id: users[0].id,
                        email: users[0].email,
                        firstName: users[0].firstName,
                        lastName: users[0].lastName,
                        isAdmin: users[0].isAdmin,
                        token: 'fake-jwt-token'
                    };

                    return of(new HttpResponse({ status: 200, body: body }));
                } else {
                    return throwError({ error: { message: 'نام کاربری یا رمزعبور اشتباه است' } });
                }
            }

            if (request.url.endsWith('/users/register') && request.method === 'POST') {
                const newUser = request.body;
                const hasUser = savedUsers.filter(user => { return user.email === newUser.email; }).length;
                if (hasUser) {
                    return throwError({ error: { message: 'نام کاربری "' + newUser.email + '" در حال حاضر وجود دارد' } });
                }

                newUser.id = savedUsers.length + 1;
                savedUsers.push(newUser);
                localStorage.setItem('users', JSON.stringify(savedUsers));
                return of(new HttpResponse({ status: 200 }));
            }

            if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    const urlParts = request.url.split('/');
                    const id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < savedUsers.length; i++) {
                        const user = savedUsers[i];
                        if (user.id === id) {
                            savedUsers.splice(i, 1);
                            localStorage.setItem('users', JSON.stringify(savedUsers));
                            break;
                        }
                    }
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.match(/\/users\/\d+$/) && request.method === 'PUT') {
                const newUser = request.body;
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    const urlParts = request.url.split('/');
                    const id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < savedUsers.length; i++) {
                        const user = savedUsers[i];
                        if (user.id === id) {
                            savedUsers[i] = newUser;
                            localStorage.setItem('users', JSON.stringify(savedUsers));
                            break;
                        }
                    }
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.endsWith('/users') && request.method === 'GET') {
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: savedUsers }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.endsWith('/posts') && request.method === 'GET') {
                return of(new HttpResponse({ status: 200, body: savedPosts }));
            }

            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    const urlParts = request.url.split('/');
                    const id = parseInt(urlParts[urlParts.length - 1]);
                    const matchedUsers = savedUsers.filter(user => { return user.id === id; });
                    const user = matchedUsers.length ? matchedUsers[0] : null;

                    return of(new HttpResponse({ status: 200, body: user }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.match(/\/posts\/\d+$/) && request.method === 'DELETE') {
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    const urlParts = request.url.split('/');
                    const id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < savedPosts.length; i++) {
                        const post = savedPosts[i];
                        if (post.id === id) {
                            savedPosts.splice(i, 1);
                            localStorage.setItem('posts', JSON.stringify(savedPosts));
                            break;
                        }
                    }
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.match(/\/posts\/\d+$/) && request.method === 'GET') {
                const urlParts = request.url.split('/');
                const id = parseInt(urlParts[urlParts.length - 1]);
                const matchedPosts = savedPosts.filter(post => { return post.id === id; });
                const post = matchedPosts.length ? matchedPosts[0] : null;

                return of(new HttpResponse({ status: 200, body: post }));
            }

            if (request.url.endsWith('/posts/add') && request.method === 'POST') {
                const newPost = request.body;
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    newPost.id = savedPosts.length + 1;
                    savedPosts.push(newPost);
                    localStorage.setItem('posts', JSON.stringify(savedPosts));
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            if (request.url.match(/\/posts\/\d+$/) && request.method === 'PUT') {
                const newPost = request.body;
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    const urlParts = request.url.split('/');
                    const id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < savedPosts.length; i++) {
                        const post = savedPosts[i];
                        if (post.id === id) {
                            savedPosts[i] = newPost;
                            localStorage.setItem('posts', JSON.stringify(savedPosts));
                            break;
                        }
                    }
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            return next.handle(request);
        }))
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }
}

export const fakeBackendProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackend,
    multi: true
};