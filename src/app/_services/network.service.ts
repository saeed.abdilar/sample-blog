import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as config from '../../assets/json/url.json';
import { User } from '../_models/user';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Post } from '../_models/post';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${config.default.url}/users`).pipe(first());
  }

  deleteUser(id) {
    return this.http.delete(`${config.default.url}/users/` + id);
  }

  editUser(user: User) {
    return this.http.put(`${config.default.url}/users/` + user.id, user);
  }

  addUser(user: User) {
    return this.http.post(`${config.default.url}/users/register`, user);
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${config.default.url}/posts`).pipe(first());
  }

  deletePost(id) {
    return this.http.delete(`${config.default.url}/posts/` + id);
  }

  getPost(id): Observable<Post> {
    return this.http.get<Post>(`${config.default.url}/posts/` + id);
  }

  addPost(post:Post) {
    return this.http.post(`${config.default.url}/posts/add`, post);
  }

  editPost(post: Post) {
    return this.http.put(`${config.default.url}/posts/` + post.id, post);
  }
}
