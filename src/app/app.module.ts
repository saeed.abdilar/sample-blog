import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { fakeBackendProvider, JwtInterceptor, ErrorInterceptor } from './_mocks';
import { AuthenticationService, NetworkService } from './_services';
import { AuthGuard } from './_services/auth.guard';

const MODULES = [
  AppRoutingModule,
  HttpClientModule
];

const COMPONENTS = [
  AppComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ...MODULES
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    NetworkService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
