import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ManagmentPanelComponent } from './managment-panel.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { AddPostComponent } from './add-post/add-post.component';
import { EditPostComponent } from './edit-post/edit-post.component';


const routes: Routes = [
  {
    path: '',
    component: ManagmentPanelComponent,
    children: [
      { path: 'users', component: UsersComponent },
      { path: 'posts', component: PostsComponent },
      { path: 'add', component: AddPostComponent },
      { path: 'edit/:id', component: EditPostComponent },
      { path: '', redirectTo: 'posts', pathMatch: 'full' },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagmentPanelRoutingModule { }
