import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagmentPanelRoutingModule } from './managment-panel-routing.module';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { AddPostModule } from './add-post/add-post.module';
import { EditPostModule } from './edit-post/edit-post.module';

import { ManagmentPanelComponent } from './managment-panel.component';

const MODULES = [
  ManagmentPanelRoutingModule,
  UsersModule,
  PostsModule,
  AddPostModule,
  EditPostModule
];

const COMPONENTS = [
  ManagmentPanelComponent
];

@NgModule({
  imports: [ CommonModule, ...MODULES ],
  declarations: [...COMPONENTS]
})
export class ManagmentPanelModule { }
