import { Component, OnInit, OnDestroy } from '@angular/core';
import { NetworkService, AuthenticationService } from '../../_services';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Post } from '../../_models/post';

@Component({
  selector: 'sb-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  posts: Post[] = [];

  constructor(private network: NetworkService, private authService: AuthenticationService, private route: Router) { }

  ngOnInit() {
    this.getPosts();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  logout() {
    this.authService.logout();
    this.route.navigate(['/auth']);
  }

  getPosts() {
    this.network.getPosts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.posts = response;
      }, error => {

      });
  }

  onDeleted(post: Post) {
    console.log('indel: ', post);

    this.network.deletePost(post.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.posts = JSON.parse(localStorage.getItem('posts'));
      }, error => {

      });
  }

  onEdit(post: Post) {
    this.route.navigate(['/panel/edit/' + post.id]);
  }
}
