import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';


import { PostsComponent } from './posts.component';
import { SharedModule } from '../../@shared/shared.module';

const MODULES = [
  MatCardModule,MatButtonModule,MatInputModule,SharedModule
]

const COMPONENTS = [
  PostsComponent
]

@NgModule({
  imports: [CommonModule,RouterModule, ...MODULES],
  declarations: [...COMPONENTS]
})
export class PostsModule { }
