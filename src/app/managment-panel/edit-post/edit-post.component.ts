import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NetworkService, AuthenticationService } from '../../_services';
import { Router, ActivatedRoute } from '@angular/router';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { Subject } from '../../../../node_modules/rxjs';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Post } from '../../_models/post';
import * as moment from 'jalali-moment';

@Component({
  selector: 'sb-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  @ViewChild(InputFileComponent)
  private InputFileComponent: InputFileComponent;
  title = new FormControl('', [Validators.required]);
  content = new FormControl('', [Validators.required]);
  titleValue: string;
  contentValue: string;
  post: Post;
  constructor(private network: NetworkService, private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getPost();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }

  getTitleErrorMessage() {
    return this.title.hasError('required') ? 'عنوان وارد نشده است' :
      '';
  }

  getContentErrorMessage() {
    return this.content.hasError('required') ? 'محتوا وارد نشده است' :
      '';
  }

  isValid(): boolean {
    if (!this.content.hasError('required') && !this.title.hasError('required'))
      return true;
    else
      return false
  }

  reset() {
    this.contentValue = '';
    this.titleValue = '';
    this.InputFileComponent.files = [];
  }

  save() {
    if (this.isValid()) {
      const post = {
        id: this.post.id,
        title: this.titleValue,
        content: this.contentValue,
        image: this.InputFileComponent.files[0].preview,
        date: moment().format('jYYYY/jMM/jDD')
      }
      this.network.editPost(post)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(response => {
          this.reset();
        }, error => { });
    }
  }

  getPost() {
    const id = this.route.snapshot.paramMap.get('id');
    this.network.getPost(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        if (response) {
          this.post = response;
          this.contentValue = response.content;
          this.titleValue = response.title;
        }
        else
          this.router.navigate(['/panel/posts']);
      }, error => {
      });
  }
}
