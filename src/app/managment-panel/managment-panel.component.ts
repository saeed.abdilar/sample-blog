import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sb-managment-panel',
  template: '<router-outlet></router-outlet>'
})
export class ManagmentPanelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
