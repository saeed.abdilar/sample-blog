import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NetworkService, AuthenticationService } from '../../_services';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputFileComponent } from 'ngx-input-file';
import * as moment from 'jalali-moment';
import { Subject } from 'rxjs';
import { takeUntil } from '../../../../node_modules/rxjs/operators';

@Component({
  selector: 'sb-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit, OnDestroy {
  @ViewChild(InputFileComponent)
  private InputFileComponent: InputFileComponent;
  unsubscribe$: Subject<void> = new Subject();
  title = new FormControl('', [Validators.required]);
  content = new FormControl('', [Validators.required]);
  titleValue: string;
  contentValue: string;
  constructor(private network: NetworkService, private authService: AuthenticationService, private route: Router) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  logout() {
    this.authService.logout();
    this.route.navigate(['/auth']);
  }

  add() {
    if (this.isValid()) {
      const post = {
        id: 0,
        title: this.titleValue,
        content: this.contentValue,
        image: this.InputFileComponent.files[0].preview,
        date: moment().format('jYYYY/jMM/jDD')
      }
      console.log(this.InputFileComponent.files);
      
      this.network.addPost(post)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(response => {
          this.reset();
        }, error => { });
    }
  }

  getTitleErrorMessage() {
    return this.title.hasError('required') ? 'عنوان وارد نشده است' :
      '';
  }

  getContentErrorMessage() {
    return this.content.hasError('required') ? 'محتوا وارد نشده است' :
      '';
  }

  isValid(): boolean {
    if (!this.content.hasError('required') && !this.title.hasError('required'))
      return true;
    else
      return false
  }

  reset() {
    this.contentValue = '';
    this.titleValue = '';
    this.InputFileComponent.files = [];
  }
}
