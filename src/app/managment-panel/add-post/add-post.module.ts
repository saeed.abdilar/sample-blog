import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { InputFileModule } from 'ngx-input-file';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

import { AddPostComponent } from './add-post.component';
import { InputFileConfig } from 'ngx-input-file/src/lib/interfaces/input-file-config';

const config: InputFileConfig = {};

const MODULES = [
  MatCardModule, MatButtonModule, InputFileModule.forRoot(config), MatFormFieldModule,FormsModule,ReactiveFormsModule,MatInputModule
]

const COMPONENTS = [
  AddPostComponent
]

@NgModule({
  imports: [
    CommonModule, RouterModule, ...MODULES
  ],
  declarations: [...COMPONENTS]
})
export class AddPostModule { }
