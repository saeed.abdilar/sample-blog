import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';

import { UsersComponent } from './users.component';
import { ListComponent } from './list/list.component';
import { ControllerComponent } from './controller/controller.component';

const MODULES = [
  MatTableModule,
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  ReactiveFormsModule,
  FormsModule,
  MatRadioModule
]

const COMPONENTS = [
  UsersComponent, ListComponent, ControllerComponent
]

@NgModule({
  imports: [
    CommonModule, RouterModule, ...MODULES
  ],
  declarations: [...COMPONENTS]
})
export class UsersModule { }
