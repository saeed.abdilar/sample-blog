import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { User } from '../../../_models/user';
import { UserComponent } from '../../../user/user.component';

@Component({
  selector: 'sb-controller',
  templateUrl: './controller.component.html',
  styleUrls: ['./controller.component.scss']
})
export class ControllerComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  firstName = new FormControl('', [Validators.required]);
  lastName = new FormControl('', [Validators.required]);
  isAdmin: boolean = false;
  _newUser: User = null;
  user: User = new User();
  @Output() added = new EventEmitter<User>();
  @Output() edited = new EventEmitter<User>();
  @Output() newUserChange = new EventEmitter<User>();
  @Input() set newUser(value: User) {
    if (value) {
      this.isAdmin = value.isAdmin;
      this._newUser = value;
      this.user = new User(this.newUser);
    }
  };
  get newUser(): User {
    return this._newUser;
  }
  constructor() { }

  ngOnInit() {
  }

  cancel() {
    this._newUser = null;
    this.user = new User();
    this.newUserChange.emit(this.newUser);
  }

  add() {
    if (this.isValid()) {
      this.user.isAdmin = this.isAdmin;
      this.added.emit(this.user);
      this.user = new User();
    }
  }

  edit() {
    this.user.isAdmin = this.isAdmin;
    console.log('user edit: ', this.user);
    
    this.edited.emit(this.user);
    this.user = new User();
    this._newUser = null;
  }

  changeAdminType(event) {
    event.value == 1 ? this.isAdmin = true : this.isAdmin = false;
    console.log('isAdmin: ', this.isAdmin);
  
  }

  getfirstNameErrorMessage() {
    return this.firstName.hasError('required') ? 'نام وارد نشده است' :
      '';
  }

  getlastNameErrorMessage() {
    return this.lastName.hasError('required') ? 'نام خانوادگی وارد نشده است' :
      '';
  }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'ایمیل وارد نشده است' :
      this.email.hasError('email') ? 'ایمیل وارد شده نامعتبر است' :
        '';
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'رمزعبور وارد نشده است' :
      '';
  }

  isValid() {
    if (!this.firstName.hasError('required') &&
      !this.lastName.hasError('required') &&
      !this.password.hasError('required') &&
      !this.email.hasError('required') &&
      !this.email.hasError('email')) {
      return true;
    } else {
      return false;
    }
  }
}
