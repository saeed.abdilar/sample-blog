import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../_models/user';
import { NetworkService, AuthenticationService } from '../../_services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'sb-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  users: User[] = [];
  newUser: User;

  constructor(private network: NetworkService, private authService: AuthenticationService, private route: Router) { }

  ngOnInit() {
    this.getUsers();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getUsers() {
    this.network.getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.users = response;
      }, error => {

      });
  }

  logout() {
    this.authService.logout();
    this.route.navigate(['/auth']);
  }

  posts() {
    this.route.navigate(['/panel/posts']);
  }

  onEdited(user: User) {
    this.network.editUser(user)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.users = JSON.parse(localStorage.getItem('users'));
      }, error => {

      });
  }

  onAdded(user: User) {
    this.network.addUser(user)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(response => {
      this.users = JSON.parse(localStorage.getItem('users'));
    }, error => {

    });
  }

  onDeleted(user: User) {
    this.network.deleteUser(user.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.users = JSON.parse(localStorage.getItem('users'));
      }, error => {

      });
  }
}
