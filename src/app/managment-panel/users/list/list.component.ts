import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../../_models/user';

@Component({
  selector: 'sb-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'password', 'isAdmin', 'actions'];
  _users: User[] = [];
  @Output() edit = new EventEmitter<User>();
  @Output() deleted = new EventEmitter<User>();
  @Input() set users(value: User[]) {
    if(value) {
      this._users = value;
      this._users = this._users.slice();
      console.log('user: ', this._users.length);
      
    }
  };
  get users(): User[] {
    return this._users;
  }

  constructor() { }

  ngOnInit() {
    
  }

  onEdit(event) {
    console.log('in list: ', event);
    
    this.edit.emit(event);
  }

  onDelete(event) {
    this.deleted.emit(event);
  }
}
