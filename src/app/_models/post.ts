export interface Post {
    id: number;
    image: string;
    title: string;
    date: string; 
    content: string;
}