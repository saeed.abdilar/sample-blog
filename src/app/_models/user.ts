export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    isAdmin: boolean;

    constructor(user?: User) {
        if (user) {
            this.id = user.id;
            this.firstName = user.firstName;
            this.lastName = user.lastName;
            this.email = user.email;
            this.password = user.password;
            this.isAdmin = user.isAdmin;
        }
    }
}