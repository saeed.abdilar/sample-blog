import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';

import { LoginModule } from './login/login.module';

import { AuthComponent } from './auth.component';

const MODULES = [
  AuthRoutingModule,
  LoginModule
];

const COMPONENTS = [
  AuthComponent
];

@NgModule({
  imports: [ CommonModule, ...MODULES ],
  declarations: [...COMPONENTS]
})
export class AuthModule { }
