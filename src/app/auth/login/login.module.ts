import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';

import { LoginComponent } from './login.component';

const MODULES = [
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  ReactiveFormsModule,
  FormsModule
]

const COMPONENTS = [
  LoginComponent
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ...MODULES
  ],
  declarations: [...COMPONENTS]
})
export class LoginModule { }
