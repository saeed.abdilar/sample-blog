import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { User } from '../../_models/user';
import { AuthenticationService } from '../../_services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'sb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  emailValue: string;
  passValue: string;

  constructor(private auth: AuthenticationService,
              private route: Router) { }

  ngOnInit() {
  }

  /**
   * End all open server services
   */
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  /**
   * Displays an email error if it exists.
   */
  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'ایمیل وارد نشده است' :
      this.email.hasError('email') ? 'ایمیل وارد شده نامعتبر است' :
        '';
  }

  /**
   * Displays an password error if it exists.
   */
  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'رمزعبور وارد نشده است' :
      '';
  }

  /**
   * Request login to the admin panel
   */
  login() {
    this.auth.login(this.emailValue, this.passValue)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(ress => {
        this.route.navigate(['/panel/posts']);
      }, error => {

      });
  }
}
