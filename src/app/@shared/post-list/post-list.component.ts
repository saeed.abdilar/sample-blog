import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../_models/post';
import { Router } from '@angular/router';

@Component({
  selector: 'sb-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @Output() delete = new EventEmitter<Post>();
  @Output() edit = new EventEmitter<Post>();
  @Input() set inPanel(value) {
    this._inPanel = value
    console.log('inPanel: ', this._inPanel);
  };
  get inPanel() {
    return this._inPanel;
  }
  @Input() posts: Post[] = [];
  _inPanel = 'true';
  constructor(private route: Router) { }

  ngOnInit() {
  }

  openPost(id) {
    this.route.navigate(['/user/post/' + id]);
  }

  onDelete(post: Post) {
    this.delete.emit(post);
  }

  onEdit(post: Post) {
    this.edit.emit(post);
  }
}
