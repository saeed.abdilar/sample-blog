import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

import { PostListComponent } from './post-list/post-list.component';

const MODULES = [
  MatCardModule,
  MatButtonModule
]

const COMPONENTS = [
  PostListComponent
];

@NgModule({
  imports: [
    CommonModule, ...MODULES
  ],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
