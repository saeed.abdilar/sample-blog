import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService, NetworkService } from '../../_services';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Post } from '../../_models/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  posts: Post[] = [];

  constructor(private network: NetworkService, private authService: AuthenticationService, private route: Router) { }

  ngOnInit() {
    this.getPosts();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getPosts() {
    this.network.getPosts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        this.posts = response;
        console.log('posts: ', this.posts);

      }, error => {

      });
  }
}
