import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { SharedModule } from '../../@shared/shared.module';

import { PostsComponent } from './posts.component';

const MODULES = [
    MatCardModule,
    MatButtonModule,
    SharedModule
]

const COMPONENTS = [
  PostsComponent
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ...MODULES
  ],
  declarations: [...COMPONENTS]
})
export class PostsModule { }
