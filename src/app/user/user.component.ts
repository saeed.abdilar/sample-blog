import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sb-user',
  template: '<router-outlet></router-outlet>'
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
