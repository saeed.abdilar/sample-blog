import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { PostsModule } from './posts/posts.module';
import { DetailModule } from './detail/detail.module';

import { UserComponent } from './user.component';

const MODULES = [
  UserRoutingModule,
  PostsModule,
  DetailModule
];

const COMPONENTS = [
  UserComponent
]

@NgModule({
  imports: [CommonModule, ...MODULES],
  declarations: [...COMPONENTS]
})
export class UserModule { }
