import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserComponent } from './user.component';
import { PostsComponent } from './posts/posts.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      { path: 'posts', component: PostsComponent },
      { path: 'post/:id', component: DetailComponent },
      { path: '', redirectTo: 'posts', pathMatch: 'full' }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
