import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NetworkService } from '../../_services';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Post } from '../../_models/post';

@Component({
  selector: 'sb-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject();
  post: Post;
  constructor(private route: ActivatedRoute, private network: NetworkService, private router: Router) { }

  ngOnInit() {
    this.getPost();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getPost() {
    const id = this.route.snapshot.paramMap.get('id');
    this.network.getPost(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(response => {
        if (response)
          this.post = response;
        else  
        this.router.navigate(['/user/posts']);
    }, error => {
          console.log('err', error);

        });
  }
}
